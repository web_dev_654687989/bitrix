<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
		
</div><!-- end of page wrapper -->

<script src="<?=SITE_TEMPLATE_PATH?>/libs/jquery/jquery-3.4.1.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/libs/bootstrap/bootstrap.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/libs/swiper/swiper.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/libs/maskedinput/jquery.maskedinput.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/libs/fancybox/jquery.fancybox.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/libs/validator/jquery.validate.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/libs/scroll2id/pagescroll2id.min.js"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/common.js"></script>

<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<script src="<?=SITE_TEMPLATE_PATH?>/js/map.js"></script>		

</body>
</html>