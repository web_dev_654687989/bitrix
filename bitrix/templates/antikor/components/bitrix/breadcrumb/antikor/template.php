<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//delayed function must return a string

__IncludeLang(__DIR__.'/lang/'.LANGUAGE_ID.'/'.basename(__FILE__));

$curPage = $GLOBALS['APPLICATION']->GetCurPage($get_index_page=false);

if ($curPage != SITE_DIR)
{
	if (empty($arResult) || $curPage != $arResult[count($arResult)-1]['LINK'])
		$arResult[] = array('TITLE' =>  htmlspecialcharsback($GLOBALS['APPLICATION']->GetTitle(false, true)), 'LINK' => $curPage);
}

if(empty($arResult))
	return "";
	
$strReturn = '<ul class="breadcrumb">';

for($index = 0, $itemSize = count($arResult); $index < $itemSize; $index++)
{
	//$strReturn .= '<i>&ndash;</i>';

	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	
	if($arResult[$index]["LINK"] <> ""&&$index<(count($arResult)-1))
		$strReturn .= '<li class="breadcrumb-item" id="bx_breadcrumb_'.$index.'" '.($index > 0? ' itemprop="child"' : '').' itemref="bx_breadcrumb_'.($index+1).'">
					<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="url">
						'.$title.'
					</a>
			</li>';
	else
		$strReturn .= '<li class="breadcrumb-item 11"  id="bx_breadcrumb_'.$index.'">'.$title.'</li>';
}

$strReturn .= '</ul>';

return $strReturn;
?>