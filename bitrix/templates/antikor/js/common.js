$(document).ready(function() {

	// SANDWICH ANIMATION
	$(".toggle_menu").click(function() {
		$(".toggle_menu").toggleClass("active");
		$(".main_header .nav_block").slideToggle();
	});


	// HOVER DROPDOWN
	$(".nav .dropdown").hover(function(){
		if($(window).width() > 991) {
			$(this).children(".dropdown_menu").stop().slideDown(200).addClass("open");
			$(this).addClass("open");
		}
	},
	function(){
		if($(window).width() > 991) {
			$(".dropdown_menu", this).stop().slideUp(200).removeClass("open");
			$(this).removeClass("open");
		}
	});

	$(".main_header .nav .dropdown > a").click(function(){
		if($(window).width() < 992) {
			$(this).next(".dropdown_menu").slideToggle(200).toggleClass("open");
			$(this).parent().toggleClass("open");
			return false;
		}
	});


	// FIXED NAVBAR
	if($(window).width() > 1200) {
		header = $(".main_header .top_header").height();
	} else {
		header = $(".main_header .top_header").height() - 34;
	}

	$(window).resize(function(){
		if($(window).width() > 1200) {
			header = $(".main_header .top_header").height();
		} else {
			header = $(".main_header .top_header").height() - 34;
		}
	});
	
	if($(this).scrollTop() > header) {
		$('.main_header .nav_block').addClass('fixed_nav');
	}
	else{
		$('.main_header .nav_block').removeClass('fixed_nav');
	}
	$(function(){
		$(window).scroll(function() {
			if($(this).scrollTop() > header) {
				$('.main_header .nav_block').addClass('fixed_nav');
			}
			else{
				$('.main_header .nav_block').removeClass('fixed_nav');
			}
		});
	});


	// TELL SANDWICH
	$(".mobile_tel_btn").click(function() {
		$(".mobile_tel_btn").toggleClass("open");
		$(".header_tels").slideToggle();
	});


	// SLIDE TO TOP
	if ($(this).scrollTop() > 700) {
		$('#to_top').addClass("active")
	} else {
		$('#to_top').removeClass("active")
	}
	$(window).scroll(function () {
		if ($(this).scrollTop() > 700) {
			$('#to_top').addClass("active")
		} else {
			$('#to_top').removeClass("active")
		}
	});
	$('#to_top').click(function() {
		$("html, body").animate({ scrollTop: 0 }, 400);
	});


	// FANCYBOX
	$().fancybox({
		loop: true,
		infobar: true,
		animationEffect: "zoom"
	});

  
	// SPOILER
	$(".spoiler_item .spoiler").click(function() {
		if ($(this).parent().hasClass("active")) {
			$(this).next().collapse('hide');
			$(this).parent().removeClass("active");
		} else {
			$(this).next().collapse('toggle');
			$(this).parent().toggleClass("active");
		}
		
	});


	// CHANGE AUTO TYPE
	$(".change_cars_type .change_btn").click(function() {
		$(this).toggleClass("active");
		$(".change_cars_type .cars_list").slideToggle();
	});

	$(".change_cars_type .cars_list li a").click(function() {
		var text = $(this).text();
		var attr = $(this).find(".red_car").attr("src");
		$(".change_cars_type .cars_list li a, .change_cars_type .change_btn").removeClass("active");
		$(this).addClass("active");
		$(".change_cars_type .cars_list").slideUp();
		$(".change_cars_type .active_car").text(text);
		$(".change_cars_type .change_btn .car_img img").attr("src", attr);
	});




	// PHONE INPUT MASK
	$(function(){
		$("input[name='tel']").mask("+7 (999) 999 99 99");
	});



	// GALLERY SLIDER
	$('.slider_wrapper').each(function() {
		var thisThumbs = $(this).find('.gallery_thumbnails');
		var thisSlider = $(this).find('.gallery_slider');
	
		var galleryThumbs = new Swiper(thisThumbs, {
			direction: getDirection(),
			slidesPerView: 6,
			slidesPerColumn: 2,
			spaceBetween: 18,
			watchSlidesVisibility: true,
			watchSlidesProgress: true,
			on: {
				resize: function () {
					galleryThumbs.changeDirection(getDirection());
				}
			},
			breakpoints: {
				992: {
					slidesPerColumn: 2,
					slidesPerView: 6,
					spaceBetween: 18,

				},
				768: {
					slidesPerView: 5,
					spaceBetween: 18,
					slidesPerColumn: 1,
		
				},
				575: {
					slidesPerView: 4,
					spaceBetween: 18,
					slidesPerColumn: 1,
		
				},
				300: {
					slidesPerView: 3,
					spaceBetween: 18,
					slidesPerColumn: 1,
		
				},
			}
		});

		function getDirection() {
			var windowWidth = window.innerWidth;
			var direction = window.innerWidth <= 991 ? 'horizontal' : 'vertical';
			return direction;
		}
	
		var gallerySlider = new Swiper(thisSlider, {
			slidesPerView: 1,
			spaceBetween: 0,
			loop: true,
			watchSlidesVisibility: true,
			watchSlidesProgress: true,
			navigation: {
				nextEl: '.next_slide',
				prevEl: '.prev_slide',
			},
			thumbs: {
				swiper: galleryThumbs,
			},
		});
	});


	// CENTERS SLIDER
	$('.center_slider').each(function() {
		var centersSwiper = new Swiper(this, {
			slidesPerView: 4,
			spaceBetween: 30,
			watchSlidesVisibility: true,
			watchSlidesProgress: true,
			navigation: {
				nextEl: '.next_slide',
				prevEl: '.prev_slide',
			},
			breakpoints: {
				1200: {
					slidesPerView: 4,
					spaceBetween: 30,
				},
				992: {
					slidesPerView: 3,
					spaceBetween: 30,
				},
				768: {
					slidesPerView: 2,
					spaceBetween: 30,
				},
				480: {
					slidesPerView: 2,
					spaceBetween: 15,
				},
				300: {
					slidesPerView: 1,
					spaceBetween: 15,
				},
			}
		});
	});


	
	// SCROLL TO ID
	$(".schemes_wrapper .item a[href*='#']").mPageScroll2id({
		scrollSpeed: 500,
		offset: 50
	});



	// MAIN FORM
	$('.question_form, .main_form').each(function() {
		$(this).validate({
			rules:{
				name: {
					
				},
				tel: {
					minlength: 6,
				},
				email: {
					email: true
				},
				messages: {
					required: true,
				},
			},
			messages:{
				name: {
					required: "Поле не заполнено"
				},
				tel: {
					required: "Поле не заполнено",
					minlength: "Мало символов"
				},
				email: {
					required: "Поле не заполнено",
					email: "Введен некорректный email"
				},
				meessage: {
					required: "Поле не заполнено"
				},
			},
			submitHandler: function(form) {
				$.ajax({
					url: "php/submit.php",
					type: "POST",
					data: $(form).serialize(),
					success: function(response) {
						$(this).trigger('reset');
						$(".modal").modal("hide");
						setTimeout(function(){$('#thanks_modal').modal("show")}, 500); 
					}            
				});
			}
		});
	});



	// STARS
	$('.stars_list_change li').hover(
		function(){ 
			$(this).addClass('hover');
			$(this).prevAll().addClass('hover');
		},
		function(){ 
			$(this).removeClass('hover');
			$(this).prevAll().removeClass('hover');
		}
	);
	$(".stars_list_change li").click(function() {
		var index = $(this).attr("data-index");
		$(".review_modal .ocenka").text(index);
		$(this).addClass("active");
		$(this).nextAll().removeClass('active');
		$(this).prevAll().addClass('active');
	});


});