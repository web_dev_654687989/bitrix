$(document).ready(function() {

	// CHANGE AUTO TYPE
	$(".calculator .change_btn").click(function() {
		$(this).toggleClass("active");
		$(".calculator .cars_list").slideToggle();
	});
	$(document).mouseup(function (e) {
		if ($(".calculator .change_btn").hasClass("active")) {
			var container = $(".calculator .multi_select");
			if (container.has(e.target).length === 0){
				$(".calculator .change_btn").removeClass("active");
				$(".calculator .cars_list").slideUp();
			}
		}
	});

	$(".calculator .cars_list li").click(function() {
		var text = $(this).text();
		var attr = $(this).find(".red_car").attr("src");
		$(".calculator .cars_list li, .calculator .change_btn").removeClass("active");
		$(this).addClass("active");
		$(".calculator .cars_list").slideUp();
		$(".calculator .change_btn .text p").text(text);
		$(".calculator .change_btn .car_img img").attr("src", attr);
	});



	$(".calculator .types_list li").click(function() {
		$(".calculator .types_list li").removeClass("active");
		$(this).addClass("active");
	});

	

});