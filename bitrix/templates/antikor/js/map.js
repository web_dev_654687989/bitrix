// YANDEX MAP
ymaps.ready(init);
var myMap, 
	myPlacemark;
function init(){ 
	myMap = new ymaps.Map("map", {
		center: [55.770215, 37.622504],
		zoom: 11,
		controls: []
	}); 
	if($(window).width() > 767) {
        myMap.setZoom(11);
    } else {
        myMap.setZoom(10);
    };
    $(window).resize(function() {
	       if($(window).width() > 767) {
	        myMap.setZoom(11);
	    } else {
	        myMap.setZoom(10);
	    };
    });

	myMap.controls.add(new ymaps.control.ZoomControl({options: { position: { right: 10, top: 50 }}}));

	myPlacemark1 = new ymaps.Placemark([55.847609, 37.504708], {
		balloonContentHeader: 'ул. Кронштадтский б-р, д. 35 Б',
		balloonContentBody: '<a href="img/scheme_1.jpg" class="fancybox">Сехма проезда</a>',
		iconCaption: 'Антикор'
	}, {
		iconLayout: 'default#image',
		iconImageHref: 'img/map_mark.png',
		iconImageSize: [110, 121],
		iconImageOffset: [-40, -105]
	});

	myPlacemark2 = new ymaps.Placemark([55.873178, 37.597127], {
		balloonContentHeader: 'Север Москвы: Высоковольтный пр. 9',
		balloonContentBody: '<a href="img/scheme_2.jpg" data-fancybox="" class="fancybox">Сехма проезда</a>',
		iconCaption: 'Антикор'
	}, {
		iconLayout: 'default#image',
		iconImageHref: 'img/map_mark.png',
		iconImageSize: [110, 121],
		iconImageOffset: [-40, -105]
	});

	myPlacemark3 = new ymaps.Placemark([55.626101, 37.621534], {
		balloonContentHeader: 'Юг Москвы: Варшавское ш. 125 стр.20 ',
		balloonContentBody: '<a href="img/scheme_3.jpg" data-fancybox="" class="fancybox">Сехма проезда</a>',
		iconCaption: 'Антикор'
	}, {
		iconLayout: 'default#image',
		iconImageHref: 'img/map_mark.png',
		iconImageSize: [110, 121],
		iconImageOffset: [-40, -105]
	});

	myPlacemark4 = new ymaps.Placemark([55.733518, 37.553810], {
		balloonContentHeader: 'Центр: Бережковская наб. д.20 стр. 23 ',
		balloonContentBody: '<a href="img/scheme_4.jpg" data-fancybox="" class="fancybox">Сехма проезда</a>',
		iconCaption: 'Антикор'
	}, {
		iconLayout: 'default#image',
		iconImageHref: 'img/map_mark.png',
		iconImageSize: [110, 121],
		iconImageOffset: [-40, -105]
	});

	myPlacemark5 = new ymaps.Placemark([55.707598, 37.445554], {
		balloonContentHeader: 'ул. Генерала Дорохова, <br>д.2, стр.2',
		balloonContentBody: '<a href="img/scheme_5.jpg" data-fancybox="" class="fancybox">Сехма проезда</a>',
		iconCaption: 'Антикор'
	}, {
		iconLayout: 'default#image',
		iconImageHref: 'img/map_mark.png',
		iconImageSize: [110, 121],
		iconImageOffset: [-40, -105]
	});

	myPlacemark6 = new ymaps.Placemark([55.775241, 37.746229], {
		balloonContentHeader: 'ул. 1-я ул. Измайловского зверинца, д. 8',
		balloonContentBody: '<a href="img/scheme_6.jpg" data-fancybox="" class="fancybox">Сехма проезда</a>',
		iconCaption: 'Антикор'
	}, {
		iconLayout: 'default#image',
		iconImageHref: 'img/map_mark.png',
		iconImageSize: [110, 121],
		iconImageOffset: [-40, -105]
	});

	myMap.behaviors.disable('multiTouch');
	myMap.behaviors.disable('scrollZoom');
	myMap.geoObjects.add(myPlacemark1);
	myMap.geoObjects.add(myPlacemark2);
	myMap.geoObjects.add(myPlacemark3);
	myMap.geoObjects.add(myPlacemark4);
	myMap.geoObjects.add(myPlacemark5);
	myMap.geoObjects.add(myPlacemark6);
	
	var isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i);
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i);
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i);
		},
		Opera: function() {
			return navigator.userAgent.match(/Opera Mini/i);
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i);
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
		}
	};
	if(isMobile.any()){
		myMap.behaviors.disable('drag');
	}
}


