<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?=LANGUAGE_ID?>" lang="<?=LANGUAGE_ID?>">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" type="image/x-icon" href="<?=SITE_TEMPLATE_PATH?>/img/favicon.png" />
<link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|PT+Sans:400,400i,700,700i&display=swap&subset=cyrillic" rel="stylesheet">

<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/libs/bootstrap/bootstrap.min.css" />
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/libs/fancybox/jquery.fancybox.min.css" />
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/libs/swiper/swiper.min.css" />
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/main.css" />
<link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/media.css" />

<?$APPLICATION->ShowHead();?>
<title><?$APPLICATION->ShowTitle()?></title>

</head>
<body>
	<div id="panel"><?$APPLICATION->ShowPanel();?></div>

	<!-- PAGE WRAPPER -->
		<div class="page_wrapper">



		<!-- MAIN HEADER (добавляем класс index_header - только для главной страницы, добавляем класс with_video - если есть фоновое видео) -->
		<header class="main_header">

		<!-- menu -->
		<div class="nav_block">
			<div class="container">
				<?$APPLICATION->IncludeComponent(
					"bitrix:menu", 
					"antikor_multilevel", 
					//"horizontal_multilevel", 
					array(
						"ROOT_MENU_TYPE" => "top",
						"MENU_CACHE_TYPE" => "A",
						"MENU_CACHE_TIME" => "3",
						"MENU_CACHE_USE_GROUPS" => "N",
						"MENU_CACHE_GET_VARS" => array(
						),
						"MAX_LEVEL" => "2",
						"CHILD_MENU_TYPE" => "second_lvl",
						"USE_EXT" => "N",
						"DELAY" => "N",
						"ALLOW_MULTI_SELECT" => "N",
						"COMPONENT_TEMPLATE" => "antikor_multilevel"
					),
					false
				);?>
			</div>
		</div>
		<!-- end of menu -->
		</header>
		<!-- END MAIN HEADER -->
		
		
		<!-- INNER PAGE HEADER -->
		<section class="inner_page_header">
			<div class="container">
				<h2 class="page_title">
					<?if($APPLICATION->GetCurDir() != SITE_DIR):?>
						<?$APPLICATION->ShowTitle(false)?>
					<?endif;?>
				</h2>
				
<?php
$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb", 
	"antikor", 
	array(
		"START_FROM" => "0",
		"PATH" => "",
		"SITE_ID" => "s1",
		"COMPONENT_TEMPLATE" => "antikor"
	),
	false
);
?>
				
				
			</div>
		</section>
		<!-- END INNER PAGE HEADER -->
	